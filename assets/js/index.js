// Déclaration des variables et assignation avec le DOM
let boutonrechercherVille = document.querySelector('#boutonrechercherVille').addEventListener('click', rechercherVille);
let conteneur = document.querySelector(".conteneur");
let conteneurMeteo = document.querySelector("#conteneurMeteo");

// Création d'une fonction permettant la recherche d'une ville grâce à l'API de openweathermap
function rechercherVille(event) {
    event.preventDefault();
    
    let saisiRechercheVille = document.getElementById('saisiRechercheVille').value; // On récupère la valeur saisie par l'utilisateur
    fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${saisiRechercheVille}&appid=773f29388f42acb8d6185bd7f99482b8&&lang=fr&units=metric`)
        .then(function (response) {
            if (response.ok) {
                console.log("OK"); // Test d'un OK en console
                return response.json();
            } else {
                // On créer un message pour indiquer un erreur de requête  
                let messageErreur = document.createElement("h2");
                messageErreur.setAttribute("id", "erreurMessage");
                messageErreur.innerHTML = "Veuillez saisir un nom correct.";
                conteneurMeteo.append(messageErreur);
                //Dès lors que l'utilateur reclique sur le bouton le message se supprime.
                boutonrechercherVille = document.querySelector('#boutonrechercherVille').addEventListener('click', nettoyerPage);
            }
        })
        .then(function (data) {

            // On appel la fonction charger d'afficher les informations météorologique de la ville saisi.
            affichageInformation(data);
            
            // Lorsque l'on recliquera sur le bouton pour rechercher, on efface la page actuelle.
            boutonrechercherVille = document.querySelector('#boutonrechercherVille').addEventListener('click', nettoyerPage);
        })
        .catch(function (error) {
            console.log(error);
        })
}
// Création d'une fonction permettant d'afficher les informations météorologique de la ville saisi. Oui elle fait plus de 15 lignes, désoler.
function affichageInformation(data) {
    for (let index = 0; index < data.list.length; index++) {

        //Déclaration d'un élément HTML, div, on définit un attribut sur ce dernier
        let informationMeteo = document.createElement("div");
        informationMeteo.setAttribute("id", "informationMeteo");
        //On associe notre élément HTML créée, au conteneurMeteo de notre page HTML 
        conteneurMeteo.append(informationMeteo);

        let enTeteConteneur = document.createElement("div");
        enTeteConteneur.setAttribute("id", "enTeteConteneur");
        //On associe notre élément HTML créee, à notre conteneur (informationMeteo), précédement créee.
        informationMeteo.append(enTeteConteneur);

        //Déclaration d'un élément HTML, p, on définit un attribut sur ce dernier
        let villeSaisiUtilisateur = document.createElement("p");
        villeSaisiUtilisateur.setAttribute("class", "elementEnTete");
        //On affecte le nom de la ville puis son code pays à notre variable villeSaisiUtilisateur afin de l'afficher
        villeSaisiUtilisateur.innerHTML = data.city.name + ", " + data.city.country;

        let nouvelleFormatDate = formatDate(data.list[index].dt_txt);  //Création d'une variable stockant une fonction permettant de formatter la date/heure
        let dateJour = document.createElement("p");
        dateJour.setAttribute("class", "elementEnTete");
        dateJour.innerHTML = 'Date : ' + nouvelleFormatDate;

        let tempsMeteo = document.createElement("p");
        tempsMeteo.setAttribute("class", "elementEnTete");
        tempsMeteo.innerHTML = data.list[0].weather[0].description;

        //On imbrique dans notre conteneur "en-tête" précédemment créée les éléments qui seront en en-tête du conteneur.
        enTeteConteneur.append(villeSaisiUtilisateur, tempsMeteo, dateJour);

        // Ici on créer toutes les variables qui apporteront de l'information pour l'utilisateur
        let temperatureVille = document.createElement("p");
        temperatureVille.innerHTML = 'Température : ' + data.list[index].main.temp + ' °C';

        let temperatureVilleMin = document.createElement("p");
        temperatureVilleMin.innerHTML = 'Température Min : ' + data.list[index].main.temp_min + ' °C';

        let temperatureVilleMax = document.createElement("p");
        temperatureVilleMax.innerHTML = 'Température Max : ' + data.list[index].main.temp_max + ' °C';

        let pressionVille = document.createElement("p");
        pressionVille.innerHTML = 'Pression : ' + data.list[index].main.pressure + ' hPa';

        let humiditeVille = document.createElement("p");
        humiditeVille.innerHTML = 'Humidité : ' + data.list[index].main.humidity + ' %';

        //Comme précédemment, on imbrique dans notre conteneur "corps" les informations pour l'utilisateur
        informationMeteo.append(enTeteConteneur, temperatureVille, temperatureVilleMin, temperatureVilleMax, pressionVille, humiditeVille);
    }
}

// Cette fonction permet de nettoyer la page afin "d'actualiser la lecture des éléments".
function nettoyerPage (element) {
    while(conteneurMeteo.firstChild) {
        conteneurMeteo.removeChild(conteneurMeteo.firstChild);
    }
}

// Cette fonction permet de changer le format de la date/heure de la météo récupérer.
function formatDate(date) {
    let dateFormat = new Date(date).toLocaleString('fr-FR', {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    });
    return dateFormat;
}